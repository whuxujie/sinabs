# Contributing to sinabs

We welcome developers to build and contribute to sinabs.

Please contact sadique.sheik at aictx dot ai for a contributors license agreement. 



## How do you go about it?

Short answer: fork, make changes, merge request to sinabs.

**Develop from**: `master` branch of sinabs.


The most straight-forward workflow to contribute would be to fork the repository and make your changes.
Once you finalize your changes, please create a merge request.

Please see gitlab's explanation on [Forking Workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html) 
for a detailed explanation.

## Coding style

Please adhere to the coding style of the library when you develop your contributions.
We use pep8 + black code style and formatting. 
(A more detailed document on code style will soon be published)
