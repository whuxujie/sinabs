.. SINABS documentation master file, created by
   sphinx-quickstart on Fri Mar 22 18:58:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SINABS's documentation!
==================================

SINABS - Sinabs Is Not A Brain Simulator

Table of contents
-----------------

.. toctree::
   gettingstarted
   introduction
   examples
   api


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
