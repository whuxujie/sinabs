sinabs.from_torch
===================

This module provides support for importing models into the sinabs from pytorch.
It currently only has limited capability.


.. toctree::
    :maxdepth: 3
    :caption: from_torch



.. py:currentmodule:: sinabs.from_torch
.. automodule:: sinabs.from_torch
    :members:
